<?php
// ------------------------------------------------------------------------
// |@Author       : Jarmin <edshop@qq.com>
// |@----------------------------------------------------------------------
// |@Date         : 2022-12-26 17:05:44
// |@----------------------------------------------------------------------
// |@LastEditTime : 2022-12-26 17:21:06
// |@----------------------------------------------------------------------
// |@LastEditors  : Jarmin <jarmin@ladmin.cn>
// |@----------------------------------------------------------------------
// |@Description  : 
// |@----------------------------------------------------------------------
// |@FilePath     : VchartsTool.php
// |@----------------------------------------------------------------------
// |@Copyright (c) 2022 http://www.ladmin.cn   All rights reserved. 
// ------------------------------------------------------------------------
declare (strict_types = 1);
namespace think\components\vcharts;

use think\admin\QuickTool;

class VchartsTool extends QuickTool
{
    /**
     * @return array
     */
    public function script(): array
    {
        return [
            'vchart'=> __DIR__.'/../dist/js/field.js'
        ];
    }

    /**
     * @return array
     */
    public function style(): array
    {
        return [
            'vchart'=> __DIR__.'/../dist/css/field.css'
        ];
    }
}