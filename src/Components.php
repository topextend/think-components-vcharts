<?php
// ------------------------------------------------------------------------
// |@Author       : Jarmin <edshop@qq.com>
// |@----------------------------------------------------------------------
// |@Date         : 2022-12-26 16:08:54
// |@----------------------------------------------------------------------
// |@LastEditTime : 2022-12-26 17:21:12
// |@----------------------------------------------------------------------
// |@LastEditors  : Jarmin <jarmin@ladmin.cn>
// |@----------------------------------------------------------------------
// |@Description  : 
// |@----------------------------------------------------------------------
// |@FilePath     : Components.php
// |@----------------------------------------------------------------------
// |@Copyright (c) 2022 http://www.ladmin.cn   All rights reserved. 
// ------------------------------------------------------------------------
declare (strict_types = 1);
namespace think\components\vcharts;

use think\Service;

/**
 * 模块注册服务
 * Class Components
 * @package think\components\vcharts
 */
class Components extends Service
{
    public function register()
    {
        //
    }

    /**
     * 启动服务
     */
    public function boot()
    {
        //
    }
}